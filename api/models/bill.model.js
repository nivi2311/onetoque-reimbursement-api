'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

 var billSchema = new Schema({
    employee_username: {
        type: String,
        required: 'Kindly enter the name of the employee'
      },
      mail_id:{
        type: String
      },
      employee_name: {
        type: String,
        required: 'Kindly enter the employee name'
      },
    invoice_number: {
      type: String,
      required: 'Kindly enter the invoice_number'
    },
    bill_date: {
      type: String,
      required: 'Kindly enter the bill_date'
    },
    submitted_date: {
      type: Date
    },
    amount: {
        type: Number,
        required: 'Kindly enter the amount'
      },
    filename : {
        type : String
    },
    filedata : {
        type : Buffer,
        required: 'Kindly enter the filedata'
    },
      type: {
        type: String,
        required: 'Kindly give type of reimbursement'
      },
      bill_month:{
        type: String,
        required: 'Kindly enter the month'
      },
      next_approval:{
        type: String
      },
    approval_timeline: {
      type: [{
        changed_by: {
        type: String
        },
        timestamp: {
            type: Date
          }
      }]
    }
  }, {collection: 'billInfo'});
billSchema.index({employee_username:1, invoice_number:1}, { unique: true });
billSchema.index({employee_username:1, bill_month:1, type:1}, { unique: true });
billSchema.index({submitted_date:1});
module.exports = mongoose.model('Bills',billSchema);
