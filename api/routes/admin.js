var express = require('express');
var router = express.Router();
var admin = require('../Controllers/admin');
var auth = require('../Controllers/auth')

//Search bills based on input - invoice number/employee number/employee name/employee username/bill month/next approval
router.get('/search_history/:str/:pageNo',auth.verifyToken, admin.searchBillHistory);

//View requests based on the role of requester
router.get('/view_requests',auth.verifyToken, admin.viewRequests);

//Get the details of the bill with given invoice number
router.get('/get_bill_details/:invoice_number/:username',auth.verifyToken, admin.viewBillDetails);

//View all the previous bills of the sent username
router.get('/view_bill_status/:username/:type/:role',auth.verifyToken, admin.viewBillStatus);

//Approve bill and send it as a request to the next admin in flow
router.post('/approve_bill',auth.verifyToken, admin.approveBillDetails);

//Decline the bill and delete the bill from the DB
router.post('/decline_bill',auth.verifyToken,admin.declineBill);

//Download the merged pdf of excel and the bill
router.post('/download',auth.verifyToken, admin.downloadExcelPdf);

module.exports = router;
