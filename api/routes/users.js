var express = require('express');
var router = express.Router();
var user = require('../Controllers/users');
var auth = require('../Controllers/auth')

/* GET users listing. */
router.get('/hello', function(req, res, next) {
  res.send("respond with a resource");
});

//login
router.post('/login', user.userlogin);
router.get('/logout',auth.verifyToken, user.logout);

//Insert Bills to Mongo
router.get('/insert_bills',auth.verifyToken, user.insertBillsView);
router.post('/insert_bills',auth.verifyToken, user.insertBills);



//View all the previous bills of the sent username
router.get('/view_bill_status/:username/:type',auth.verifyToken, user.viewBillStatus);

//View bill pdf of the given invoice number
router.post('/view_pdf',auth.verifyToken, user.viewpdf);

//Delete the bill and delete the bill from the DB
router.post('/decline_bill',auth.verifyToken,user.declineBill);

module.exports = router;
