'use strict'
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var express = require('express');
var moment = require('moment');
var merge = require('easy-pdf-merge');
var path = require('path');
var fs = require('fs');
var bills = require('../models/bill.model');
var pdfmake = require('pdfmake');
var redis = require('redis');
const nodemailer = require('nodemailer');


var flowdata = fs.readFileSync('./flow.json');
var flow = JSON.parse(flowdata);

var configdata = fs.readFileSync('./config.json');
var config = JSON.parse(configdata);

var redisClient = redis.createClient({host : 'redis', port : 6379});

//Required Variables & functions
global.secret = 'Secret';

//Function to generate directory with random number
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

//Function to delete the directory and the files
function deleteFolder(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        let curPath = path + "/" + file;
          fs.unlinkSync(curPath);
      });
      fs.rmdirSync(path);
    }
  };

var admins= flow.roles.Finance.admins;
var admin_list = admins.concat(flow.roles.HR.admins);
console.log(admin_list);


//APIs

//View Bills based invoice number/employee number/employee name/employee username/bill month/next approval

exports.searchBillHistory = function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            if(admin_list.indexOf(data.emp_username)!= -1){
                console.log(data.emp_username + ' entered View Bill History API');
                if(!req.params.str || req.params.str==undefined)
                //Checks if parameter is not empty
                    res.send({'success': false,"message": "Invalid parameter"});
                else{
                    var page = req.params.pageNo || 1
                    //If the parameter is a number, the it is converted to Integer and compared with employee number field else number will be 0
                    let num = parseInt(req.params.str);
                    console.log(num);
                    console.log(req.params.str);
                    if (isNaN(num)){
                        num = 0;
                    }
                    bills.count({ $or:[ { 'employee_name': RegExp(req.params.str, "i") }, { 'employee_username': RegExp(req.params.str, "i")},{ 'invoice_number': RegExp(req.params.str, "i")},{ 'next_approval': RegExp(req.params.str, "i") },{ 'bill_month': RegExp(req.params.str, "i")}, { 'type': RegExp(req.params.str,"i") } ]})
                    .exec(function(error,n){
                        if(n){
                            var count = n;
                            bills.find({ $or:[ { 'employee_name': RegExp(req.params.str, "i") }, { 'employee_username': RegExp(req.params.str, "i")},{ 'invoice_number': RegExp(req.params.str, "i")},{ 'next_approval': RegExp(req.params.str, "i") },{ 'bill_month': RegExp(req.params.str, "i")}, { 'type': RegExp(req.params.str,"i") } ]} , {  'filedata': 0, 'approval_timeline' : 0, '_id' : 0, 'filename': 0, '__v': 0, 'submitted_date': 0   }).skip((10 * page) - 10)
                            .limit(10)
                            .exec(function(err,bill){
                                    if(bill){
                                        // If matches are found, documents are returned
                                        console.log(bill);
                                        res.send({"count": count, "bills": bill});
                                    }
                                    else {
                                        // If error, error message is sent
                                        console.log("no bills found");
                                        res.send({'success': false,'message': 'no bills found'});                   
                                }
                            })
                        }      
                    else{
                        console.log("no bills found");
                        res.send({'success':false, 'message':'no bills found'});
                        }
                    })
                }
            }else{
                console.log(data.emp_username + " is not an admin");
                res.send({'success': false,'message': 'not and admin'});
            }      
        }
    });
};

//View Requests API for Finance home page and HR home page

exports.viewRequests =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            if(admin_list.indexOf(data.emp_username)!= -1){
                console.log(data.emp_username + ' entered ViewRequests API');
                if(data.role == "Finance"){
                    let date = moment(new Date());
                    let limit_date = new Date(Date.parse(date.month()+1 + ' 24,' + date.year()));
                    console.log(limit_date);
                    //IF it is a finance request, then both finance and oracle requests are sent
                    bills.find({ $or: [{ $and: [{ 'employee_username': { $ne : data.emp_username }}, { 'next_approval': "Finance" } ]}, { 'next_approval': "Oracle"}] }, { 'filedata': 0, 'approval_timeline' : 0, '_id' : 0, 'filename': 0, '__v': 0  }).sort({submitted_date:1})
                    .exec(function(err,doc){
                        if(doc){
                            var docs = []
                            for(let i=0;i<doc.length;i++){
                                var dates = doc[i].bill_month.split('-');
                                var d = new Date(Date.parse(dates[0] + ' 1, ' + dates[1]));
                                console.log(d);
                                if(d < limit_date)
                                docs.push(doc[i]);
                            }
                            console.log("The bill details are \n " + docs);
                            res.send(docs);
                        }
                        else {
                            res.send({'success': false,'message': err});
                        }
                    })
                }else{
                    let date = moment(new Date());
                    let limit_date = new Date(Date.parse(date.month()+1 + ' 24,' + date.year()));
                    //IF it is an HR request then requests with Next approval as HR will be sent
                    bills.find({ $and: [{ 'employee_username': { $ne : data.emp_username}},{'next_approval': data.role }]}, { 'filedata': 0, 'approval_timeline' : 0, '_id' : 0, 'filename': 0, '__v': 0  }).sort({submitted_date:1})
                    .exec(function(err,doc){
                        if(doc){
                            var docs = []
                            for(let i=0;i<doc.length;i++){
                                if(new Date(doc[i].bill_date)<= limit_date)
                                docs.push(doc[i]);
                            }
                            console.log("The bill details are \n " + docs);
                            res.send(docs);
                        }
                        else {
                            res.send({'success': false,'message': err});
                        }
                    })
                }
            }
            else{
                console.log(data.emp_username + " is not an admin");
                res.send({'success': false,'message': 'not and admin'});
            }
        }
    })   
};

exports.downloadExcelPdf = function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    if(!req.body.admin_username || !req.body.invoice_number){
        res.send({'success': false,"message": "Empty username or Empty number"});
        console.log(req.body.admin_username);
        console.log(req.body.employee_username);
    }
    else{
        var fonts = {
            Roboto: {
                normal: 'fonts/Roboto-Regular.ttf',
                bold: 'fonts/Roboto-Medium.ttf',
                italics: 'fonts/Roboto-Italic.ttf',
                bolditalics: 'fonts/Roboto-MediumItalic.ttf'
            }
        };
        let PdfPrinter = require('../../node_modules/pdfmake/src/printer');
        let printer = new PdfPrinter(fonts);
        redisClient.hgetall(token,function(rerr,user) {
            if(rerr){
                console.log(rerr);
            }                
            else {
                if(admin_list.indexOf(user.emp_username)!= -1){
                    console.log(user.emp_username  + " entered download API");
                    var invoice = req.body.invoice_number;
                    var invoice_number = invoice.split('@');
                    let next_approval;
                    if(req.body.request_type == "approve")
                        next_approval = "Oracle";
                    else
                        next_approval = "Approved";
                    bills.find( {$and:[{ 'invoice_number': RegExp(invoice_number[0])},{'type': req.body.type},{'employee_username':req.body.employee_username},{'next_approval': next_approval}]}).sort({submitted_date:1})
                    .exec(function(error,bill){
                        if(bill){
                            //IF the bill with given invoice number is present in DB
                        
                            var rows=[];
                            rows.push(['Item No','Submitted Date', 'Date', 'Bill Month','Expenses','Cost','Receipt','VAT','Currency','Total']);
                            for (let i=0; i< bill.length; i++){
                                let date = moment(bill[i].submitted_date);
                                rows.push([i+1,date.format("DD-MMM-YYYY"),bill[i].bill_date,bill[i].bill_month,req.body.type,bill[i].amount,'yes','','INR',bill[i].amount]);
                            }

                            var approval_rows =[];
                            approval_rows.push(['',flow.flow.Internet[0], '',flow.flow.Internet[1],''])
                            approval_rows.push(['Bill Month','Approved By','Approved Time','Approved By','Approved Time'])
                            for (let i=0; i< bill.length; i++){
                                let time1 = moment(bill[i].approval_timeline[0].timestamp).utcOffset("+05:30").format("MMMM Do YYYY h:mm:ss a");
                                let time2 = moment(bill[i].approval_timeline[1].timestamp).utcOffset("+05:30").format("MMMM Do YYYY h:mm:ss a");
                                approval_rows.push([bill[i].bill_month,bill[i].approval_timeline[0].changed_by,time1 ,bill[i].approval_timeline[1].changed_by,time2]);
                            }
                            
                            console.log(rows);
                            console.log(approval_rows);
                            var bill_months;
                            var amounts;
                            console.log(bill[0]);
                            //console.log(invoice_number[0]);
                            let docDefinition = {
                                content: [
                                    {text: 'Reimbursement of Expenses',color:'white',fontSize: 14, background: 'black', style: 'subheader'},
                                    '\n\n',
                                    {text: 'Employee Name\t\t : '+ bill[0].employee_name, fontSize: 9}, 
                                    {
                                        text:
                                        [{text:  'Expense Report\t\t  :', fontSize: 9},{text : req.body.type+' Reimbursement', background:'yellow',fontSize: 9}]
                                    },
                                    {text: 'Invoice Number\t\t  : '+ invoice_number[0], fontSize: 9},
                                    {text: 'Email_ID\t\t\t           : ' + bill[0].mail_id, fontSize: 9},
                                    '\n',
                                    {text: 'Expenses', style: 'subheader'},
                                    //'The following table has nothing more than a body array',
                                    { style: 'table',
                                    table: {
                                        body: rows
                                    }
                                    },

                                    '\n',
                                    {text: 'Approval Summary', style: 'subheader'},
                                    //'The following table has nothing more than a body array',
                                    {
                                        style: 'table',
                                        table: {
                                            body: approval_rows
                                        }
                                    },
                                ],
                                styles: {
                                    header: {
                                        fontSize: 10,
                                        margin: [0, 0, 0, 10],
                                        alignment: 'center'
                                    },
                                    subheader: {
                                        fontSize: 9,
                                        margin: [0, 10, 0, 5],
                                        color: 'white',
                                        background: 'black'
                                    },
                                    table: {
                                        fontSize: 9,
                                        margin: [0, 5, 0, 15],
                                        alignment: 'center'
                                    },
                                    tableHeader: {
                                        fontSize: 10,
                                        color: 'black',
                                        alignment: 'center'
                                    }
                                }
                            };
                            console.log(docDefinition);
                            let rand = getRandomInt(10000000000);
                            let dir = './' + rand;

                            if (!fs.existsSync(dir)){
                                fs.mkdirSync(dir);
                            }
                            console.log(bill[0].filename);
                            //pdfmake.createPdf(docDefinition).download();
                            let pdfDoc = printer.createPdfKitDocument(docDefinition);
                            pdfDoc.pipe(fs.createWriteStream( dir + '/' + bill[0].filename+'_excel.pdf'));
                            pdfDoc.end();
                            console.log("PDF file written");
                        
                            let filename = dir + '/' + bill[0].filename +'.pdf';
                            let file = filename;

                            // Convert Binary bill data to pdf to merge and download
                            let data =  bill[0].filedata;
                            fs.writeFile(file, data,'binary', function(err) {
                                if(err)
                                console.log(err);
                                else
                                console.log("The pdf file was saved!", file);
                            }); 

                            
                            // Merge the two pdfs and save it in the form username_year_month.pdf
                            merge([dir + '/' + bill[0].filename +'.pdf', dir + '/' + bill[0].filename+"_excel.pdf"], dir + '/' + bill[0].filename +'.pdf',function(err){             
                                if(err)
                                return console.log("Merge Error"+ err);                            
                                console.log('Successfully merged!'); 
                                res.contentType('application/pdf');
                                res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                                //var data = fs.readFileSync(file);
                                res.download(file, function(err){
                                    if(!err){
                                        deleteFolder(dir);
                                        console.log('Deleted final');
                                    }
                                });     
                                    
                            });
                        }
                        else{ 
                            //Invalid invoice number
                            res.send({'success': false,'message': "Invalid invoice number"});
                            console.log("Invalid invoice number");
                        }
                    });
                }
                else{
                    console.log(user.emp_username + " is not an admin");
                    res.send({'success': false,'message': 'not and admin'});

                }
            }
        });
    }
}

// Update/Approve API
exports.approveBillDetails =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            if(admin_list.indexOf(data.emp_username)!= -1){
                console.log(data.emp_username + ' entered approveBill API');

                if(!req.body.old_invoice_number || !req.body.new_invoice_number || !req.body.amount || !req.body.bill_month || !req.body.employee_username || !req.body.role)
                {
                        //Check if body contains all the fields
                        console.log("Approval failure");
                        res.send({'success': false, 'error': "All fields are necessary"});
                }
                else{
                    
                    //The person who has to next approve is found using the role of the person who has approved now and the person next in JSON flow
                    function findNext(role){
                        return role === req.body.role;
                    }    
                    var index = flow.flow.Internet.findIndex(findNext);
                    console.log(index);
                    //The details of the bill are updated and also the next approval is changed
                    if (req.body.role == 'Oracle'){
                        var invoice = req.body.old_invoice_number;
                        var invoice_number = invoice.split("@");
                        bills.updateMany({$and:[{'invoice_number': RegExp(invoice_number[0]) },{'next_approval':'Oracle'},{'employee_username': req.body.employee_username}]}, {'next_approval':'Approved'})
                        .exec(function(error,doc){
                            if(doc){
                                bills.find({$and:[{'invoice_number':RegExp(invoice_number[0]) },{'next_approval':'Approved'},{'employee_username': req.body.employee_username}]},{'filedata':0})
                                .exec(function(err,bills){
                                    console.log(bills);
                                    if(err || bills == {} || bills == []){ 
                                        //Invalid invoice number
                                        res.send({'success': false,'message': "Invalid invoice number"});
                                        console.log("Invalid invoice number");
                                    }
                                    else{
                                        var months ='';
                                        for(let i=0;i<bills.length;i++){
                                            if (bills.length==1)
                                            months = months + bills[i].bill_month
                                            else if(i == bills.length -1)
                                            months = months + 'and ' + bills[i].bill_month
                                            else
                                            months = months + bills[i].bill_month + ', '

                                        }
                                        nodemailer.createTestAccount((err, account) => {
                                            // create reusable transporter object using the default SMTP transport
                                            let transporter = nodemailer.createTransport({
                                                host: 'vcasmtp.hq.corp.viasat.com',
                                                port: 25,
                                                secure: false // true for 465, false for other port
                                            });          
                                            // setup email data with unicode symbols
                                            let mailOptions = {
                                                from: 'onetoque@viasat.com', // sender address
                                                to: bills[0].mail_id, // list of receivers
                                                subject: bills[0].type+' Reimbursement request for bill dated '+bills[0].bill_date+'  approved', // Subject line
                                                html: '<p>Hi '+ bills[0].employee_name + ',</p><p>Your request for '+bills[0].type+' reimbursement for the bill '+ invoice_number[0]+' dated '+bills[0].bill_date+' for the month/s of '+ months +' has been successfully approved and your bill will be reimbursed within 15 days from now.</p><br/><br/> Regards,<br/>OneToque', // html body,
                                                tls: {
                                                rejectUnauthorized: false
                                            }
                                            };  
                                            // send mail with defined transport object
                                            transporter.sendMail(mailOptions, (error, info) => {
                                                if (error) {
                                                    return console.log(error);
                                                }
                                                console.log('Message sent: %s', info.messageId);
                                                res.send({"success": true});
                                            });
                                        });
                                    }
                                })
                            }else{
                                console.log("Update failure");
                                res.send({'success': false, 'error': error});     
                            }
                        })
                    }else{
                        var new_invoice = req.body.new_invoice_number.split('@');
                        bills.update({$and:[{'invoice_number' : req.body.old_invoice_number},{'employee_username':req.body.employee_username},{'next_approval':req.body.role}]}, { 'invoice_number': req.body.new_invoice_number , 'amount' : req.body.amount, 'bill_month': req.body.bill_month, 'next_approval': flow.flow.Internet[index+1] , $push : {'approval_timeline' : { 'changed_by': req.body.admin_username, 'timestamp': new Date() }} })
                        .exec(function(error,doc){
                            if(doc){
                                console.log("The number of bills updated is/are :" + doc.n);
                                bills.findOne({$and:[{'invoice_number' : req.body.new_invoice_number},{'employee_username':req.body.employee_username}]},{'filedata':0})
                                .exec(function(err,bills){
                                    console.log(bills);
                                    //let str = JSON.stringify(bill);
                                    //let bills = JSON.parse(str);
                                    if(err || !bills){ 
                                        //Invalid invoice number
                                        res.send({'success': false,'message': "Invalid invoice number"});
                                        console.log("Invalid invoice number");
                                    }
                                    else if(bills.next_approval != 'Oracle' && bills.next_approval != 'Approved'){
                                        console.log(bills);
                                        nodemailer.createTestAccount((err, account) => {
                                            // create reusable transporter object using the default SMTP transport
                                            let transporter = nodemailer.createTransport({
                                                host: 'vcasmtp.hq.corp.viasat.com',
                                                port: 25,
                                                secure: false // true for 465, false for other port
                                            });          
                                            // setup email data with unicode symbols
                                            let mailOptions = {
                                                from: 'onetoque@viasat.com', // sender address
                                                to: bills.mail_id, // list of receivers
                                                subject: bills.type+' Reimbursement request for '+bills.bill_month+' approved by finance', // Subject line
                                                html: '<p>Hi '+ bills.employee_name + ',</p><p>Your request for '+bills.type+' reimbursement for the bill '+ req.body.new_invoice_number+' dated '+bills.bill_date+' for the month of '+ bills.bill_month +' has been successfully approved by the '+req.body.role+' team and is sent for '+flow.flow.Internet[index+1]+' Approval</p><br/><br/> Regards,<br/>OneToque', // html body,
                                                tls: {
                                                rejectUnauthorized: false
                                            }
                                            };  
                                            // send mail with defined transport object
                                            transporter.sendMail(mailOptions, (error, info) => {
                                                if (error) {
                                                    return console.log(error);
                                                }
                                                console.log('Message sent: %s', info.messageId);
                                                res.send({"success": true});
                                            });
                                        });
                                    }
                                    else{
                                        console.log('Sent to Oracle upload');
                                        res.send({"success": true});
                                    }
                                })      
                            }
                            else{           
                                console.log("Update failure");
                                res.send({'success': false, 'message': error});
                            }
                        })
                    }    
                }
            } 
            else{
                console.log(data.emp_username + " is not an admin");
                res.send({'success': false,'message': 'not and admin'});

            }   
        }
    })   
};


//View bill details API

exports.viewBillDetails =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            if(admin_list.indexOf(data.emp_username)!= -1){
                console.log(data.emp_username + ' entered bill details API');
                if(!req.params.invoice_number)
                {
                        //Check if parameter is not empty
                        console.log("Empty parameter");
                        res.send({'success': false, 'message': "All fields are necessary"});
                }
                else{
                    
                    //The details of a particular bill with requested invoice number is fetched from db and sent.
                    console.log(req.params.invoice_number);
                    bills.findOne({$and:[{ 'invoice_number': req.params.invoice_number},{'employee_username':req.params.username}]}, { 'bill_month': 1, 'invoice_number':1, 'amount':1 , 'employee_name':1, 'employee_number' :1, 'employee_username':1, 'filename':1, 'next_approval':1, 'bill_date':1, 'type' :1  })
                    .exec(function(err,doc){
                        if(doc){
                            console.log("The bill details are :\n" + doc);
                            res.send(doc);               
                        }
                        else {
                            res.send({'success': false,'message': "Invalid invoice number"});
                        }
                    })
                };
            }
            else{
                console.log(data.emp_username + " is not an admin");
                res.send({'success': false,'message': 'not and admin'});

            }
        }
    });
}

//Decline API Delete the bill from the 
exports.declineBill =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            if(admin_list.indexOf(data.emp_username)!= -1){
                console.log(data.emp_username + ' entered declineBill API');
                if(!req.body.invoice_number)
                {
                        //Check if parameter is not empty
                        console.log("Empty body");
                        res.send({'success': false, 'message': "All fields are necessary"});
                }
                else{        
                    //The details of the bill with the given invoice number is deleted from the database
                    bills.findOne({$and:[{'invoice_number' : req.body.invoice_number},{'employee_username': req.body.employee_username}]})
                    .exec(function(err,bill){
                        if(bill){
                            bills.remove({$and:[{'invoice_number' : req.body.invoice_number},{'employee_username': req.body.employee_username}]})
                            .exec(function(error,doc){
                                if(doc.n==1){
                                    console.log(bill);
                                    nodemailer.createTestAccount((err, account) => {
                                        // create reusable transporter object using the default SMTP transport
                                        let transporter = nodemailer.createTransport({
                                            host: 'vcasmtp.hq.corp.viasat.com',
                                            port: 25,
                                            secure: false // true for 465, false for other port
                                        });          
                                        // setup email data with unicode symbols
                                        let mailOptions = {
                                            from: 'onetoque@viasat.com', // sender address
                                            to: bill.mail_id, // list of receivers
                                            subject: bill.type+' Reimbursement request for '+bill.bill_month+' declined', // Subject line
                                            html: '<p>Hi '+ bill.employee_name + ',</p><p>Your request for '+bill.type+' reimbursement for the bill '+req.body.invoice_number+' dated '+bill.bill_date+' for the month of '+ bill.bill_month +' has been declined due to the following reason:</p><br/> '+ req.body.message +' Please upload the corrected bill.<br/><br/> Regards,<br/>OneToque', // html body,
                                            tls: {
                                            rejectUnauthorized: false
                                        }
                                        };  
                                        // send mail with defined transport object
                                        transporter.sendMail(mailOptions, (error, info) => {
                                            if (error) {
                                                return console.log(error);
                                            }
                                            console.log('Message sent: %s', info.messageId);
                                            res.send({'success': true, 'message': "The document is deleted"});
                                        });
                                    });
                                }
                                else {
                                    console.log("Delete failure");
                                    res.send({'success': false, 'message': error});
                                }
                            })                
                        }
                        else {
                            console.log("Bill not found");
                            res.send({'success': false, 'message': err});
                        }
                    })
                }
            }
            else{
                console.log(data.emp_username + " is not an admin");
                res.send({'success': false,'message': 'not and admin'});
            }
        }
    });
};


//API to view all previous bill history & its Status of users

exports.viewBillStatus =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            console.log(data.emp_username + '  entered ViewStatus API');
            if(!req.params.username){
                //checks is parameter not empty
                console.log("Empty username");
                res.send({'success': false,"message": "Empty username"});
            }
            else{
                //Sends the details of all the bills and its status of a particular user
                console.log(req.params.username);
                if (req.params.role == 'Finance'){
                    bills.find({ $and:[{ 'employee_username': req.params.username},{'type': req.params.type},{ 'next_approval': { $ne : req.params.role }}]}, { 'bill_month': 1, 'invoice_number':1, 'amount':1 , 'next_approval':1 }).sort({_id: -1}).limit(12) 
                    .exec(function(err,doc){
                    if(doc){
                        console.log("The bill details are :\n" + doc);
                        res.send(doc);
                    }
                    else {
                        console.log("Error" + err);
                        res.send({'success': false,'message': err});
                    }
                  })
                }
                else{
                    bills.find({ $and:[{ 'employee_username': req.params.username},{'type': req.params.type},{ 'next_approval': 'Approved'}]}, { 'bill_month': 1, 'invoice_number':1, 'amount':1 , 'next_approval':1 }).sort({_id: -1}).limit(12) 
                    .exec(function(err,doc){
                    if(doc){
                        console.log("The bill details are :\n" + doc);
                        res.send(doc);
                    }
                    else {
                        console.log("Error" + err);
                        res.send({'success': false,'message': err});
                    }
                  })
                }  
            }
        }
    })
};
