'use strict'
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var express = require('express');
var ldap = require('ldapjs');
var jwt = require('jsonwebtoken');
var formidable = require('formidable');
var moment = require('moment');
var path = require('path');
var fs = require('fs');
var bills = require('../models/bill.model');
var redis = require('redis');
var nodemailer = require('nodemailer')

var merge = require('easy-pdf-merge');

var pdfmake = require('pdfmake');

var flowdata = fs.readFileSync('./flow.json');
var flow = JSON.parse(flowdata);

var configdata = fs.readFileSync('./config.json');
var config = JSON.parse(configdata);

var redisClient = redis.createClient({host : 'redis', port : 6379});

//Required Variables & functions
global.secret = 'Secret';

//Insert bills HTML Page

exports.insertBillsView =  function(req, res) {
    res.sendFile(path.join(__dirname + '../../../public/bill_upload.html'));
};

//Insert bills API to insert in database

exports.insertBills =   function(req,res){
    console.log("Entered insert bills API");
    let token = req.body.token || req.query.token || req.headers['authorization'];
    let form = new formidable.IncomingForm();


    form.parse(req, function(err, fields, files){
        if(!files.gymbill){
            //IF any field is missing, throws error
            console.log("bill empty");
            res.send({"success" : false, "message" : "bill empty"});
        }else if(!files.attendancesheet){
            //IF any field is missing, throws error
            console.log("attendence sheet empty");
            res.send({"success" : false, "message" : "attendence sheet empty"});
        }else if(!fields.username){
            //IF any field is missing, throws error
            console.log("username empty");
            res.send({"success" : false, "message" : "username empty"});
        }else if(!fields.invno){
            //IF any field is missing, throws error
            console.log("Empty invoice number");
            res.send({"success" : false, "message" : "invoice number empty"});
        }else if(!fields.date){
            //IF any field is missing, throws error
            console.log("date empty");
            res.send({"success" : false, "message" : "date empty"});
        }else if(!fields.amount){
            //IF any field is missing, throws error
            console.log("amount empty");
            res.send({"success" : false, "message" : "amount empty"});
        }
        else{
            // If all fields are present proceeds to save in database
            
            
            let date = new Date(fields.date);
            
            //Stores the filename in the format of username_year_month
            
            
            
            let filename = fields.username ;
          
            merge([files.gymbill.path, files.attendancesheet.path], './' + filename ,function(err){             
               if(err)
               return console.log("Merge Error"+ err);                            
               console.log('Successfully merged!');                                           
              
            
                console.log("merged file"+filename);
                
                //Checks if it is a registered user
                redisClient.hgetall(token,function(err,data) {
                    if(err){
                        console.log(err);
                    }                
                    else {
                        console.log(data);
                        //If it is a valid user proceeds to insert
                        moment.createFromInputFallback = function(config) {
                            // unreliable string magic, or
                            config._d = new Date(config._i);
                        };
                        var date_moment = moment(date);
                        console.log( date_moment.format("DD-MMM-YYYY"));
                        var start_month = moment(fields.startmonth)
                            console.log(start_month.format("DD-MMM-YYYY"));
                            console.log(fields.frequency)
                        var multiple_bills = [];
                        var months = '';
                        for(var i=1;i<=fields.frequency;i++){
                        
                            console.log('i'+i);
                            var bill = new bills({
        
                                employee_name: data.emp_name,
                                employee_username: fields.username,
                                mail_id: data.mail,
                                invoice_number : fields.invno+'@'+start_month.format("MMMM-YYYY"),
                                bill_date : date_moment.format("DD-MMM-YYYY"),
                                bill_month : start_month.format("MMMM-YYYY"),
                                type : fields.type,
                                submitted_date : new Date(),
                                amount : fields.amount,
                                filename : filename+ '_' +start_month.format("MMMM-YYYY"),
                                next_approval : flow.flow.Internet[0],
                                
                                filedata : fs.readFileSync('./'+filename)
                            });
                            start_month =start_month.add(1,'M');
                            console.log('bill');
                            console.log(bill);
                            
                            multiple_bills.push(bill);
                            
                            if(fields.frequency == 1)
                                months = months + start_month.format("MMMM-YYYY")
                            else if (i == fields.frequency)
                                months = months + 'and ' + start_month.format("MMMM-YYYY");
                            else 
                                months = months + start_month.format("MMMM-YYYY") + ', ';
                            
                                
                        }

                            //IF it is a duplicate invoice number, mongo error is thrown
                        bills.insertMany(multiple_bills, function(er,doc){
                        //IF it is a duplicate invoice number, mongo error is thrown
                            if(doc){
                                console.log(doc);
                                console.log("saved");
                                nodemailer.createTestAccount((err, account) => {
                                    // create reusable transporter object using the default SMTP transport
                                    let transporter = nodemailer.createTransport({
                                        host: 'vcasmtp.hq.corp.viasat.com',
                                        port: 25,
                                        secure: false // true for 465, false for other port
                                    });          
                                    // setup email data with unicode symbols
                                    let mailOptions = {
                                        from: 'onetoque@viasat.com', // sender address
                                        to: data.mail, // list of receivers
                                        subject: 'Gym bill reimbursement claim', // Subject line
                                        html: '<p>Hi '+ data.emp_name + ',</p><p>Your request for '+fields.type+' reimbursement for the bill '+fields.invno+' dated '+date_moment.format("DD-MMM-YYYY") +' for the month/s of '+months+' has been successfully submitted and is waiting to for Finance Approval</p><br/><br/> Regards,<br/>OneToque', // html body,
                                        tls: {
                                        rejectUnauthorized: false
                                    }
                                    };  
                                    // send mail with defined transport object
                                    transporter.sendMail(mailOptions, (error, info) => {
                                        if (error) {
                                            return console.log(error);
                                        }else{
                                        console.log('Message sent: %s', info.messageId);
                                        res.send({'success':true});
                                        }
                                    });
                                });   
                            } 
                            else {
                               
                                console.log("Duplicate Invoice Number/Bill month"+ er);
                                res.send({'success':false, 'message':"Duplicate Invoice number/Bill month"});
                            }
                        })   
                    }
                })
            })
        };
    })
};





