'use strict'
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var express = require('express');
var ldap = require('ldapjs');
var jwt = require('jsonwebtoken');
var formidable = require('formidable');
var moment = require('moment');
var path = require('path');
var fs = require('fs');
var bills = require('../models/bill.model');
var redis = require('redis');
const nodemailer = require('nodemailer');

var flowdata = fs.readFileSync('./flow.json');
var flow = JSON.parse(flowdata);

var configdata = fs.readFileSync('./config.json');
var config = JSON.parse(configdata);

var redisClient = redis.createClient({host : 'redis', port : 6379});

//Required Variables & functions
global.secret = 'Secret';

//Login API

exports.userlogin = function(req,res){
    console.log("entered Login API");

    //Check if body is not empty
    if((req.body.username) && (req.body.password)){
        console.log(req.body.username);
        let tlsOptions = { 'rejectUnauthorized': false };
        let client = ldap.createClient({
        url: config.url,
        tlsOptions: config.tlsOptions
        });
        client.bind('uid=' + req.body.username + ',ou=People,dc=viasat,dc=io', req.body.password , function(error){
            //Binds with the LDAP server
            if(!error){
                // If there is no error, the login success and creates token
                console.log(req.body.username +" entered bind")
                client.search('uid=' + req.body.username + ',ou=People,dc=viasat,dc=io', function(err, response) {
                    //assert.ifError(err);
                    response.on('searchEntry', function(entry) {
                        console.log('entry: ' + JSON.stringify(entry.object));
                        let token = jwt.sign({id: entry.object.uid},secret , {
                            expiresIn: 1000
                            })
                            console.log('success');
                            let role,editable;
                            if (flow.roles.Finance.admins.includes(entry.object.uid)){
                                role = flow.roles.Finance.role_name;
                                editable = flow.roles.Finance.editable;
                            }
                                
                            else if (flow.roles.HR.admins.includes(entry.object.uid)){
                                role = flow.roles.HR.role_name;
                                editable = flow.roles.HR.editable;
                            }
                            else{
                                role = "User";
                                editable = false;
                            }
                            redisClient.hmset(token,{"emp_name" : entry.object.cn, "emp_username": entry.object.uid, "role": role, "editable": editable, "mail": entry.object.mail});
                            redisClient.expire(token, 300);
                            redisClient.hgetall(token,function(err,reply) {
                                console.log(err);
                                console.log(reply);
                               });
                            res.send({"success":true, "token": token, "userdata": { "emp_name" : entry.object.cn, "emp_username": entry.object.uid, "role": role, "editable": editable }});       
                            
                    });
                    response.on('error', function(err) {
                        console.error('error: ' + err.message);
                    });
                    response.on('end', function(result) {
                        console.log('status: ' + result.status);
                    });
                });               
            }
            else{
                //If binding error occors
                res.send({ 'success': false, 'message': "Your username and password do not match" });
            }
        });           
    }
    else {
        //If the body fields are missing
        console.log("Error : Empty Username or Password field");
        res.send({ 'success': false, 'message': "username or password empty" });
    }
}

  
exports.logout = function(req,res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.del(token, function(err, response) {
        if (response == 1) {
           console.log("Deleted Successfully!")
           res.send({"success": true});
        } else{
         console.log("Cannot delete")
         res.send({"success": false, "message": "Token not deleted"});
        }
     })
}

exports.homepageController = function(req, res, next) {
    res.send('Welcome to One Toque Portal');
  };

//Insert bills HTML Page

exports.insertBillsView =  function(req, res) {
    res.sendFile(path.join(__dirname + '../../../public/bill_upload.html'));
};

//Insert bills API to insert in database

exports.insertBills =   function(req,res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            console.log(data.emp_username + " entered insert bills API");
            let form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files){
                if(!files.file){
                    //IF any field is missing, throws error
                    console.log("File empty");
                    res.send({"success" : false, "message" : "File empty"});
                }else if(!fields.username){
                    //IF any field is missing, throws error
                    console.log("username empty");
                    res.send({"success" : false, "message" : "username empty"});
                }else if(!fields.invno){
                    //IF any field is missing, throws error
                    console.log("Empty invoice number");
                    res.send({"success" : false, "message" : "invoice number empty"});
                }else if(!fields.date){
                    //IF any field is missing, throws error
                    console.log("date empty");
                    res.send({"success" : false, "message" : "date empty"});
                }else if(!fields.amount){
                    //IF any field is missing, throws error
                    console.log("amount empty");
                    res.send({"success" : false, "message" : "amount empty"});
                }
                else{
                    // If all fields are present proceeds to save in database
                    console.log("Files",files);                  
                    let date = new Date(fields.date);                  
                    //Stores the filename in the format of username_year_month
                    
        
                    console.log(data);
                    //If it is a valid user proceeds to insert
                    moment.createFromInputFallback = function(config) {
                        // unreliable string magic, or
                        config._d = new Date(config._i);
                    };
                    var date_moment = moment(date);
                    console.log( date_moment.format("DD-MMM-YYYY"));
                    
                    var start_month = moment(fields.startmonth)
                    console.log(start_month.format("DD-MMM-YYYY"));
                    let filename = fields.username + '_' + fields.date;
                    console.log(fields.frequency)
                    let error = false;
                    var months = '';
                    var multiple_bills = [];
                    for(var i=1;i<=fields.frequency;i++){
                     
                        console.log(start_month);
                    //Inserts the details to the database
                        var bill = new bills({
                            employee_name: data.emp_name,
                            employee_username: fields.username,
                            mail_id: data.mail,
                            invoice_number : fields.invno+'@'+start_month.format("MMMM-YYYY"),
                            bill_date : date_moment.format("DD-MMM-YYYY"),
                            bill_month : start_month.format("MMMM-YYYY"),
                            type : fields.type,
                            submitted_date : new Date(),
                            amount : fields.amount,
                            filename : filename,
                            next_approval : flow.flow.Internet[0],
                            filedata : fs.readFileSync(files.file.path)
                        });
                        multiple_bills.push(bill);
                        start_month =start_month.add(1,'M');
                        
                        if(fields.frequency == 1)
                            months = months + start_month.format("MMMM-YYYY")
                        else if (i == fields.frequency)
                            months = months + 'and ' + start_month.format("MMMM-YYYY");
                        else 
                            months = months + start_month.format("MMMM-YYYY") + ', ';
                       
                        console.log(start_month);  
                    }

                    bills.insertMany(multiple_bills, function(er,doc){
                        //IF it is a duplicate invoice number, mongo error is thrown
                        if(doc){
                            console.log(doc);
                            console.log("saved");
                            nodemailer.createTestAccount((err, account) => {
                                // create reusable transporter object using the default SMTP transport
                                let transporter = nodemailer.createTransport({
                                    host: 'vcasmtp.hq.corp.viasat.com',
                                    port: 25,
                                    secure: false // true for 465, false for other port
                                });          
                                // setup email data with unicode symbols
                                let mailOptions = {
                                    from: 'onetoque@viasat.com', // sender address
                                    to: data.mail, // list of receivers
                                    subject: 'Internet bill reimbursement claim', // Subject line
                                    html: '<p>Hi '+ data.emp_name + ',</p><p>Your request for '+fields.type+' reimbursement for the bill '+fields.invno+' dated '+date_moment.format("DD-MMM-YYYY") +' for the month/s of '+months+' has been successfully submitted and is waiting to for Finance Approval</p><br/><br/> Regards,<br/>OneToque', // html body,
                                    tls: {
                                    rejectUnauthorized: false
                                }
                                };  
                                // send mail with defined transport object
                                transporter.sendMail(mailOptions, (error, info) => {
                                    if (error) {
                                        return console.log(error);
                                    }else{
                                    console.log('Message sent: %s', info.messageId);
                                    res.send({'success':true});
                                    }
                                });
                            });   
                        } 
                        else {
                            error = true;
                            console.log("Duplicate Invoice Number");
                            res.send({'success':false, 'message':"Duplicate Invoice number or duplicate bill month"});
                        }
                    })   
                }
            })
        }
    })
};

//API to view the bill pdf which takes invoice_number as the parameter

exports.viewpdf = function (req, res) {
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            console.log(data.emp_username + '  entered View pdf API');
            if(!req.body.invoice_number|| !req.body.employee_username){
            //Checks if parameter is not empty
                
                res.send({'success': false,"message": "Empty invoice number"});
            }
            else{
                console.log(req.body.employee_username);
                //If it is a valid invoice number, pdf data is fetched from database and sent as a pdf file
                console.log("pdf document of " + req.body.invoice_number);
                bills.findOne({$and:[{'invoice_number' : req.body.invoice_number },{'employee_username':req.body.employee_username}]})
                .exec(function (error, doc) {
                    if (doc) {
                        //IF the doc with specified invoice number is present
                        console.log("The pdf document is \n " + doc);
                        res.contentType('application/pdf');
                        res.send(doc.filedata);          
                    }
                    else{     
                        console.log("Error" + error);
                        ////IF the doc with specified invoice number is not present, error is thrown
                        res.send({'success': false,'error': "Invalid invoice number"});
                    }
                })
            }
        }
    })
};

//API to view all previous bill history & its Status of users

exports.viewBillStatus =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            console.log(data.emp_username + '  entered ViewStatus API');
            if(!req.params.username){
                //checks is parameter not empty
                console.log("Empty username");
                res.send({'success': false,"message": "Empty username"});
            }
            else{
                //Sends the details of all the bills and its status of a particular user
                console.log(req.params.username);
                bills.find({ $and:[{ 'employee_username': req.params.username},{'type': req.params.type}]}, { 'bill_month': 1, 'invoice_number':1, 'amount':1 , 'next_approval':1 }).sort({_id: -1}).limit(12) 
                .exec(function(err,doc){
                    if(doc){
                        console.log("The bill details are :\n" + doc);
                        res.send(doc);
                    }
                    else {
                        console.log("Error" + err);
                        res.send({'success': false,'message': err});
                    }
                })
            }
        }
    })
};

//Delete bill api
exports.declineBill =   function(req, res){
    let token = req.body.token || req.query.token || req.headers['authorization'];
    redisClient.hgetall(token,function(rerr,data) {
        if(rerr){
            console.log("Redis error: "+ rerr);
        }                
        else {
            if(data.emp_username == req.body.employee_username){
                console.log(data.emp_username + ' entered declineBill API');
                if(!req.body.invoice_number)
                {
                        //Check if parameter is not empty
                        console.log("Empty body");
                        res.send({'success': false, 'message': "All fields are necessary"});
                }
                else{        
                    //The details of the bill with the given invoice number is deleted from the database
                    bills.findOne({$and:[{'invoice_number' : req.body.invoice_number},{'employee_username': req.body.employee_username}]})
                    .exec(function(err,bill){
                        if(bill){
                            bills.remove({$and:[{'invoice_number' : req.body.invoice_number},{'employee_username': req.body.employee_username}]})
                            .exec(function(error,doc){
                                if(doc.n==1){
                                    console.log(bill);
                                    nodemailer.createTestAccount((err, account) => {
                                        // create reusable transporter object using the default SMTP transport
                                        let transporter = nodemailer.createTransport({
                                            host: 'vcasmtp.hq.corp.viasat.com',
                                            port: 25,
                                            secure: false // true for 465, false for other port
                                        });          
                                        // setup email data with unicode symbols
                                        let mailOptions = {
                                            from: 'onetoque@viasat.com', // sender address
                                            to: data.mail, // list of receivers
                                            subject: 'Reimbursement request for '+bill.bill_month+' deleted', // Subject line
                                            html: '<p>Hi '+ bill.employee_name + '</p><p>Your request for '+bill.type+' reimbursement for the bill dated '+bill.bill_date+' for the month of '+ bill.bill_month +' has been declined due to the following reason</p><br/><br/> '+ req.body.message +' <br/>Please upload the corrected bill.<br/><br/> Regards,<br/>Nivethitha', // html body,
                                            tls: {
                                            rejectUnauthorized: false
                                        }
                                        };  
                                        // send mail with defined transport object
                                        transporter.sendMail(mailOptions, (error, info) => {
                                            if (error) {
                                                return console.log(error);
                                            }
                                            console.log('Message sent: %s', info.messageId);
                                            res.send({'success': true, 'message': "The document is deleted"});
                                        });
                                    });
                                }
                                else {
                                    console.log("Delete failure");
                                    res.send({'success': false, 'message': error});
                                }
                            })                
                        }
                        else {
                            console.log("Bill not found");
                            res.send({'success': false, 'message': err});
                        }
                    })
                }
            }
            else{
                console.log(data.emp_username + " is not an admin");
                res.send({'success': false,'message': 'not your bill'});
            }
        }
    });
};