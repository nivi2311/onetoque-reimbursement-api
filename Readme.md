One Toque API SERVICES

Onetoque
This project was generated with Node version 8.9.4 and Mongo DB version 3.6.1.

Development server
Extract the project and navigate to the folder in command prompt. Install the dependencies using "npm install" command. 
Start the server with the command "nodemon". Navigate to http://localhost:3000/. The app will automatically reload if you change any of the source files.

oneToque - API A backend service API in NodeJS that supports the process of bill     reimbursement in a company
Setup & Run Application prerequisite - Make sure NodeJs is installed in your computer
1.	Clone the application code in your local folder from the git repo link, https://git.viasat.com/ggnanapragasam/oneToque-API.
2.	Open command prompt and select cloned folder location.
3.	Execute "npm install" in command prompt, which will install all the dependencies
4.	Execute "npm start" to run the application.

Hosting the Application in AWS:
1.	In aws, clone the master branch of OneToque( front end ) and oneToque-API (backend).
2.	Go to oneToque-API folder and run “docker-compose up –build” command to build and host the application.


Project Folder Structure:
The project folder will have a package.json file, app.js (main) file and the api folder.
The api consists of the controllers, models and the routes. Routes will have the various routes which can be used by the user. Each route will be associated with a callback function.Model has the database schema. 
The implementation of the function logic will be present in three files inside controller folder.The user.js has the user APIs, admin.js has the admin APIs and the auth.js has the authentication middleware.

						
			
			
				

