FROM node:latest
RUN apt-get update
RUN  apt-get install -y default-jre
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
EXPOSE 8888
CMD  npm start
