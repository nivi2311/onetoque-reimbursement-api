var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var redis = require('redis');
var https = require('https');
var fs = require('fs');

var options = {
	key: fs.readFileSync("./ssl-certs/otq.key"),
	cert: fs.readFileSync("./ssl-certs/otq.crt")
}

var users = require('./api/routes/users');
var admin = require('./api/routes/admin');
var gym = require('./api/routes/gym');
//var request = require('./api/Models/db');

var app = express();  //mandatory for mongodb
var cors = require('cors')

var configdata = fs.readFileSync('./config.json');
var config = JSON.parse(configdata);

mongoose.connect(config.mongo_url);

var redisClient = redis.createClient({host : 'redis', port : 6379});

redisClient.on('ready',function() {
 console.log("Redis is ready");
});

redisClient.on('error',function() {
 console.log("Error in Redis");
});
// view engine setup
//app.set('views', path.join(__dirname,'api', 'views'));
//app.set('view engine','jade');

console.log("Listen to 3443");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', users);
app.use('/admin', admin);
app.use('/gym', gym);

var server = https.createServer(options, app);
server.listen(443);
/*app.listen(8899, function(){ //mandatory for mongodb
    console.log("Server Startted on Port 8888...");
})*/

module.exports = app;
module.exports = server;
